from flask import Flask, jsonify
from flask_cors import CORS
from API.AuthToken_API import AuthToken_Route
from functools import wraps
from Middleware.middleware import validate_request
from flask import g, request, redirect, url_for

app = Flask(__name__)

CORS(app)
app.register_blueprint(AuthToken_Route)


@app.route('/')
@validate_request
def what():
    return jsonify({"What": "Authentication",
                    "Author": "AminJamal",
                    "NickName": "Includeamin",
                    "Email": "aminjamal10@gmail.com",
                    "WebSite": "includeamin.com"})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=3001, debug=True)
